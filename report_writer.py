import os

class ReportWriter():

    def __init__(self, path):
        self.output_file = os.path.join(path, 'report.txt')
        self.classification_report = None

    def write_to_file(self):
        with open(self.output_file, 'a+') as f:
            for key, line in self.__dict__.items():
                if key is 'params':
                    for k, v in line.__dict__.items():
                        for e, g in v.items():
                            f.write('{0}: {1}{2}'.format(e, g,'\n'))
                else:
                    f.write('{0}{1}'.format(line,'\n'))


    def append_single_line(self, line):
        with open(self.output_file, 'a+') as f:
            f.write(line)

    def write_and_print(self, key, value):
        self.__setattr__(key, value)

    def __setattr__(self, key, value):
        super(ReportWriter, self).__setattr__(key, value)