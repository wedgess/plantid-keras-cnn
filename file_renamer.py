import argparse
import os
import uuid

rootdir = os.path.abspath(os.path.dirname(__file__))
script_name = os.path.basename(__file__)

def rename_files(img_dir, random_names=True):
    for directory in os.listdir(img_dir):
        if os.path.isdir(os.path.join(img_dir, directory)):
            total = 0
            class_dir = os.path.join(img_dir, directory)
            print("Renaming files is directory: {}".format(class_dir))
            for file in os.listdir(class_dir):
                if random_names:
                    new_filename = str(uuid.uuid4())
                else:
                    total += 1
                    new_filename = "{0}.{1}".format(directory, str(total).zfill(8))
                os.rename(os.path.join(class_dir, file),
                          os.path.join(class_dir, "{0}.{1}".format(new_filename, "jpg")))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Splits data from a directory into training and validation sets, images are picked at random.')
    parser.add_argument('img_dir',
                        help='The containing all images in folders.')

    args = parser.parse_args()
    img_dir = os.path.join(rootdir, args.img_dir)

    if os.path.exists(img_dir):
        rename_files(img_dir, True)
        rename_files(img_dir, False)
    else:
        print("[ERROR] Cannot rename files as directory does not exists: {}".format(img_dir))
