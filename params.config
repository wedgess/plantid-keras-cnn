# The config file for the parameters. All values should be defined as Python
# types to be parsed correctly (e.g. strings should be in quotes, tuples should
# be of the form (x, y, z), etc.

# The files that contain the training and test image paths for normal training.
# Inside directory are folders containing class names, inside these folders images contain lables as file names.
data_directory:                  "img_data_300"

# data generator test and validation directories, the split must be performed manually.
data_gen_train_directory:        "img_data_300_augmented/train"
data_gen_validate_directory:     "img_data_300_augmented/validate"

# Data specifications, images_per_class and train_data_split is needed.
number_of_classes:               10
images_per_class:                300
train_data_split:                0.8

# Dimensions of the training image: width, height, num channels.
# Number of channels should be 1 (grayscale) or 3 (RGB).
input_shape:                    (168, 168, 3)

# Training hyperparameters.
batch_size:                     5
num_epochs:                    2

# SGD hyperparameters.
learning_rate:                  8e-4
decay:                          1e-5
momentum:                       0.012

# Specify model file names to write to, these will also be used for testing.
model_json_file_name:           "model-arch.json"
model_file_name:                "model.hdf5"
model_weights_file_name:        "model-weights.h5"

test_model_json_file_name:           "builds/18-02-2018-17_25/model-arch.json"
test_model_file_name:                "builds/18-02-2018-17_25/model.hdf5"
test_model_weights_file_name:        "builds/18-02-2018-17_25/model-weights.h5"

# For testing, class_names must be seperated by spaces, only a single test image is run.
class_names: "primula_vulgaris fatsia_japonica viola viburnum daisy digitalis_purpea osteospermum primula_gold_laced_group hypericum cyclamen_persicum"
test_img:            "test_images/test_primula.jpg"
