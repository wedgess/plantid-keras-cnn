import argparse
import os
import random
from shutil import copyfile
from file_renamer import rename_files

rootdir = os.path.abspath(os.path.dirname(__file__))
script_name = os.path.basename(__file__)

def collect_validation_images(img_dir):
    data = dict()  # dictionary to store folder name as key and files in list
    for directory in os.listdir(img_dir):  # list all directories
        if os.path.isdir(os.path.join(img_dir, directory)):  # if is directory
            img_list = []  # create empty list to store random images
            total = 0
            while total < 60:  # we want 60 random images for validation set
                rand_file = random.choice(os.listdir(os.path.join(img_dir, directory)))
                if rand_file not in img_list:  # if the randomly chosen file is not in the list
                    img_list.append(rand_file)  # ad the random file
                    total += 1  # increase total
            data[directory] = img_list  # add the dictionary item with directory as key and img list as value
    return data

def collect_training_images(validation_data, img_dir):
    data = dict()
    for directory in os.listdir(img_dir):  # list all directories
        train_imgs = []
        for img in os.listdir(os.path.join(img_dir, directory)):
            if img not in validation_data[directory]:
                train_imgs.append(img)
        data[directory] = train_imgs
    return data

def copy_files_to_directory(img_dir, data, directory):
    if not os.path.exists(directory):
        os.makedirs(directory)
    for directory_name, images in data.items():
        print("Folder: {0}".format(directory_name))
        class_dir = os.path.join(directory, directory_name)
        if not os.path.exists(class_dir):
            os.makedirs(class_dir)  # create directory in validation directory
        for img in images:
            # move all images from main img_dir to validation directory
            copyfile(os.path.join(img_dir, directory_name, img), os.path.join(directory, directory_name, img))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Splits data from a directory into training and validation sets, images are picked at random.')
    parser.add_argument('img_dir',
                        help='The containing all images in folders.')

    args = parser.parse_args()
    img_dir = os.path.join(rootdir, args.img_dir)

    if os.path.exists(img_dir):
        # ensure all files are named correctly
        rename_files(img_dir, True)
        rename_files(img_dir, False)

        validation_dict = collect_validation_images(img_dir)
        training_dict = collect_training_images(validation_dict, img_dir)

        validation_dir = os.path.join(img_dir, 'validate')
        train_dir = os.path.join(img_dir, 'train')
        copy_files_to_directory(img_dir, validation_dict, validation_dir)
        copy_files_to_directory(img_dir, training_dict, train_dir)

        # finally move the split data into a new folder with original folders name and augmented_new appended to it
        new_data_dir = os.path.join(rootdir, '{0}_{1}'.format(args.img_dir, "augmented"))
        if not os.path.exists(new_data_dir):
            os.mkdir(new_data_dir)
            os.rename(validation_dir, os.path.join(new_data_dir, "validate"))
            os.rename(train_dir, os.path.join(new_data_dir, "train"))
    else:
        print("[ERROR] Cannot rename files as directory does not exists: {}".format(img_dir))
