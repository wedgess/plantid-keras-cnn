#!/bin/bash

# script to download shareable Google Drive link - Credits: https://stackoverflow.com/a/38937732/3748532

echo -e "Downloading image dataset...\n"
ggID='1a8nfBMdrBIK2vzd3DUx5xCnZ4NWuKGcu'
ggURL='https://drive.google.com/uc?export=download'
filename="$(curl -sc /tmp/gcokie "${ggURL}&id=${ggID}" | grep -o '="uc-name.*</span>' | sed 's/.*">//;s/<.a> .*//')"
getcode="$(awk '/_warning_/ {print $NF}' /tmp/gcokie)"
curl -Lb /tmp/gcokie "${ggURL}&confirm=${getcode}&id=${ggID}" -o "${filename}"
tar xzf ${filename}
rm -f ${filename}
