import itertools
import os
import re

import matplotlib.pyplot as plt
import numpy as np
from sklearn.metrics import confusion_matrix, classification_report


class AccuracyAndLossGraph:
    """
    Prints the loss and accuracy graphs for train and validation data.

    :param history: contains training history (loss & accuracy)
    :param sgd_adjustables: the title of the graphs
    :param output_dir: folder to write the graphs to
    """

    def __init__(self, history, sgd_adjustables, output_dir):
        # visualizing losses and accuracy
        self.sgd_adjustables = sgd_adjustables
        self.train_loss = history['loss']
        self.val_loss = history['val_loss']
        self.train_acc = history['acc']
        self.val_acc = history['val_acc']
        self.xc = 0
        self.output_dir = output_dir

    def set_epochs(self, epochs):
        self.xc = range(epochs)

    def draw(self, epochs):
        self.set_epochs(epochs)
        self.plot_loss()
        self.plot_accuracy()
        plt.show()

    def plot_loss(self):
        plt.figure(1, figsize=(7, 5))
        plt.plot(self.xc, self.train_loss)
        plt.plot(self.xc, self.val_loss)
        plt.xlabel('Number of Epochs')
        plt.ylabel('Loss')
        plt.title('Loss --  SGD={}'.format(self.sgd_adjustables))
        plt.grid(True)
        plt.legend(['Train', 'Validation'])
        # print plt.style.available # use bmh, classic,ggplot for big pictures
        plt.style.use(['classic'])
        plt.savefig(
            os.path.join(self.output_dir, 'train_val_loss_SGD-{}'.format(re.sub(r"[.,\s=]", '', self.sgd_adjustables))))

    def plot_accuracy(self):
        plt.figure(2, figsize=(7, 5))
        plt.plot(self.xc, self.train_acc)
        plt.plot(self.xc, self.val_acc)
        plt.xlabel('Number of Epochs')
        plt.ylabel('accuracy')
        plt.title('Accuracy --  SGD={}'.format(self.sgd_adjustables))
        plt.grid(True)
        plt.legend(['Train', 'Validation'], loc=4)
        # print plt.style.available # use bmh, classic,ggplot for big pictures
        plt.style.use(['classic'])
        plt.savefig(os.path.join(self.output_dir,
                                 'train_val_accuracy_SGD-{}'.format(re.sub(r"[.,\s=]", '', self.sgd_adjustables))))


class ConfusionMatrixGraph:
    """
    Prints the confusion matrix for a normal CNN build (no data aug)
    """

    def __init__(self, model, X_test, Y_test, classes_list, output_dir, params):
        self.model = model
        self.X_test = X_test
        self.Y_test = np.argmax(Y_test, axis=1)
        self.y_pred = np.argmax(self.model.predict(X_test), axis=1)
        self.targets = build_targets(classes_list)
        self.output_dir = output_dir
        params.write_class_names_to_config(classes_list)

    def draw(self, report_writer):
        report_writer.classification_report = classification_report(self.Y_test, self.y_pred, target_names=self.targets)
        # Compute confusion matrix
        cnf_matrix = confusion_matrix(self.Y_test, self.y_pred)
        np.set_printoptions(precision=2)
        plt.figure()
        # Plot non-normalized confusion matrix
        plot_confusion_matrix(cnf_matrix, classes=self.targets, output_dir=self.output_dir, title='Confusion matrix')
        plt.show()


class ConfusionMatrixDatGenGraph:
    """
    Prints the confusion matrix for a data generator CNN build.
    """

    def __init__(self, model, validation_generator, validation_image_count, batch_size, output_dir, params):
        self.Y_test = validation_generator
        Y_pred = model.predict_generator(self.Y_test, validation_image_count // batch_size)
        self.y_pred = np.argmax(Y_pred, axis=1)
        classes_list = self.build_class_list()
        self.targets = build_targets(classes_list)
        self.output_dir = output_dir
        params.write_class_names_to_config(classes_list)

    def build_class_list(self):
        class_list = []
        for k, v in self.Y_test.class_indices.items():
            class_list.append(k)
        return class_list

    def draw(self, report_writer):
        report_writer.classification_report = classification_report(self.Y_test.classes, self.y_pred,
                                                                    target_names=self.targets)
        # Compute confusion matrix
        cnf_matrix = confusion_matrix(self.Y_test.classes, self.y_pred)
        np.set_printoptions(precision=2)
        plt.figure()
        # Plot non-normalized confusion matrix
        plot_confusion_matrix(cnf_matrix, classes=self.targets, output_dir=self.output_dir, title='Confusion matrix')
        plt.show()


# Common methods used by both confusion matrix graphs
def build_targets(classes_list):
    list = []
    for idx, class_item in enumerate(classes_list):
        list.append('class{0}({1})'.format(idx, class_item))
    return list


# Plotting the confusion matrix
def plot_confusion_matrix(cm, classes, output_dir,
                          normalize=False,
                          title='Confusion matrix',
                          cmap=plt.cm.Blues, ):
    """
    This function prints and plots the confusion matrix.
    Normalization can be applied by setting `normalize=True`.
    """
    plt.imshow(cm, interpolation='nearest', cmap=cmap)
    plt.title(title)
    plt.colorbar()
    tick_marks = np.arange(len(classes))
    plt.xticks(tick_marks, classes, rotation=90)
    plt.yticks(tick_marks, classes)

    if normalize:
        cm = cm.astype('float') / cm.sum(axis=1)[:, np.newaxis]
        print("Normalized confusion matrix")
    else:
        print('Confusion matrix, without normalization')

    # print(cm)

    thresh = cm.max() / 2.
    for i, j in itertools.product(range(cm.shape[0]), range(cm.shape[1])):
        plt.text(j, i, cm[i, j],
                 horizontalalignment="center",
                 color="white" if cm[i, j] > thresh else "black")

    plt.tight_layout()
    plt.ylabel('True label')
    plt.xlabel('Predicted label')
    plt.savefig(os.path.join(output_dir, 'aug-confusion_matrix.jpg'))


class TestImageFeatureGraph:
    """
    Used for displaying feature graphs when testing model
    """

    def __init__(self, feature_maps, feature_map_layer, maps_filter_num, save_imgs=True):
        self.feature_maps = feature_maps
        self.feature_map_layer = feature_map_layer
        self.maps_filter_num = maps_filter_num
        self.save_imgs = save_imgs

    def draw_feature_map(self):
        import numpy as np
        num_of_featuremaps = self.feature_maps.shape[2]
        fig = plt.figure(figsize=(16, 16))
        plt.title("featuremaps-layer-{}".format(self.feature_map_layer))
        subplot_num = int(np.ceil(np.sqrt(num_of_featuremaps)))
        for i in range(int(num_of_featuremaps)):
            ax = fig.add_subplot(subplot_num, subplot_num, i + 1)
            # ax.imshow(output_image[0,:,:,i],interpolation='nearest' ) #to see the first filter
            ax.imshow(self.feature_maps[:, :, i])
            plt.xticks([])
            plt.yticks([])
            plt.tight_layout()
        plt.show()
        if self.save_imgs:
            fig.savefig("featuremaps-layer-{}.jpg".format(self.feature_map_layer))

    def draw_map_feature(self):
        # show first image(filter_num) in feature map(layer_num)
        fig = plt.figure(figsize=(16, 16))
        plt.imshow(self.feature_maps[:, :, self.maps_filter_num])
        if self.save_imgs:
            plt.savefig("featuremaps-layer-{0}-filternum-{1}.jpg".format(self.feature_map_layer, self.maps_filter_num))
