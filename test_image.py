import cv2
import numpy as np
from keras import backend as K


class TestImage:
    def __init__(self, model, params):
        self.model = model
        self.labels = params['class_names'].split()
        self.test_image = params['test_img']
        self.img_size = params['input_shape'][0]

    def predict(self):
        test_image = np.expand_dims(self.load_test_image(), axis=0)

        # Predicting the test image
        # print("Predictions: {0}".format(self.model.predict(test_image)))
        for v in self.model.predict_classes(test_image):
            print("Test image predicted as: {}".format(self.labels[v]))

        predictions = self.model.predict(test_image)[0]

        print("\n--------- Confidence level per plant -----------\n")
        for label, confidence in zip(self.labels, predictions):
            conf_level = round(float(confidence), 2) * 100
            print('{}             {}%'.format(label, conf_level))

        # choose a random layer
        layer_num = 3
        #  choose first feature of layers feature map
        filter_num = 0
        activations = self.get_featuremaps(int(layer_num), test_image)
        feature_maps = activations[0][0]

        from graphs import TestImageFeatureGraph

        tig = TestImageFeatureGraph(feature_maps, layer_num, filter_num, save_imgs=False)
        tig.draw_map_feature()

        tig.draw_feature_map()

    # Visualizing the intermediate layer
    def get_featuremaps(self, layer_idx, X_batch):
        get_activations = K.function([self.model.layers[0].input, K.learning_phase()],
                                     [self.model.layers[layer_idx].output, ])
        activations = get_activations([X_batch, 0])
        return activations

    # load the test image
    def load_test_image(self):
        # from keras.preprocessing import image
        # img = image.load_img('test10.jpg', target_size=(168, 168))
        # x = image.img_to_array(img)
        # print("======================= ANDROID LIKE ============================================================")
        # print(x[0])
        # cv2.imwrite("example.jpg", x)
        # #x = np.expand_dims(x, axis=0)

        test_image = cv2.imread(self.test_image)
        test_image = cv2.resize(test_image, (self.img_size, self.img_size))
        test_image = np.array(test_image)
        test_image = test_image.astype('float32')
        # print("======================= CV2 ORIGINAL ============================================================")
        # print(test_image[0])
        return test_image
