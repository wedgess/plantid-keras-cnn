class ModelParams(object):
    """An object that defines a model's training parameters."""

    def __init__(self):
        """Sets up default values for each of the configuration values."""
        # Initialize all required config options mapping to 'None':
        config_params = [
            'data_directory',
            'data_gen_train_directory',
            'data_gen_validate_directory',
            'number_of_classes',
            'input_shape',
            'images_per_class',
            'train_data_split',
            'model_json_file_name',
            'model_file_name',
            'model_weights_file_name',
            'test_model_json_file_name',
            'test_model_file_name',
            'test_model_weights_file_name',
            'class_names',
            'test_img'
        ]
        self._params = {param: None for param in config_params}
        # Initialize default hyperparameters:
        self._params['batch_size'] = 5
        self._params['num_epochs'] = 10
        self._params['learning_rate'] = 0.0008
        self._params['decay'] = 1e-5
        self._params['momentum'] = 0.012

    def __getitem__(self, key):
        """Overload brackets operator to get config values.

        Usage: e.g. params['classnames_file']
        Args:
          key: the key (name) of the config value.
        Returns:
          The value (if available) of that configuration parameter. If the key is
          invalid or no config value was specified, returns None.
        """
        if key in self._params:
            return self._params[key]
        return None

    def write_class_names_to_config(self, class_names):
        """
        Overwrite class names, in config file so last model can be tested with test image.
        :param class_names: list of class names
        """
        import fileinput
        import re
        config_file = fileinput.FileInput("params.config", inplace=True, backup='.bak')
        for line in config_file:
            line = re.sub('(class_names:).*', 'class_names: "{}"'.format(' '.join(class_names)), line.rstrip())
            print(line)

    def read_config_file(self, fname):
        """Reads the config parameters from the given config file.
        Args:
          fname: the filename of a correctly-formatted configuration file.
        Returns:
          False if any of the required parameters was not set.
        """
        config = open(fname, 'r')
        line_num = 0
        for line in config:
            line_num += 1
            line = line.strip()
            if len(line) == 0 or line.startswith('#'):
                continue
            parts = line.split()
            if len(parts) < 2:
                print('Error: invalid config value "{0}" on line {1} of {2}'.format(line, line_num, fname))
                continue
            key = parts[0]
            key = key.replace(':', '')
            value = ' '.join(parts[1:])
            if key in self._params:
                try:
                    self._params[key] = eval(value)
                except:
                    print('Error: invalid config value "{0}" on line {1} of {2}'.format(value, line_num, fname))
            else:
                print('Error: unknown config key "{0}" on line {1} of {2}'.format(key, line_num, fname))
        # Check that all parameters were defined.
        for key in self._params:
            if not self._params[key]:
                print('Error: config parameter "{}" was not specified.'.format(key))
                return False
        return True
