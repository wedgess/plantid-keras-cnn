from keras.layers import Convolution2D, MaxPooling2D
from keras.layers import Dense, Dropout, Flatten
from keras.models import Sequential

from model_params import ModelParams


def build_model(input_shape, num_classes):
    # Defining the model
    model = Sequential()
    # Convolution layer 1 & 2
    model.add(Convolution2D(32, (3, 3), padding='same', activation="relu", input_shape=input_shape))
    model.add(Convolution2D(32, (3, 3), activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.25))

    # Convolution layer 3 & 4
    model.add(Convolution2D(32, (3, 3), padding='same', activation="relu"))
    model.add(Convolution2D(32, (3, 3), activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.3))

    # Convolution layer 5 & 6
    model.add(Convolution2D(32, (3, 3), padding='same', activation="relu"))
    model.add(Convolution2D(32, (3, 3), activation="relu"))
    model.add(MaxPooling2D(pool_size=(2, 2)))
    model.add(Dropout(0.3))

    model.add(Flatten())  # this converts our 3D feature maps to 1D feature vectors
    # FC layer 1
    model.add(Dense(1024, activation='relu'))
    model.add(Dropout(0.5))
    # Output layer
    model.add(Dense(num_classes, activation='softmax'))

    return model


def compile_model(model, learning_rate, decay, momentum):
    # SGD adjustable values
    from keras.optimizers import SGD

    sgd = SGD(lr=learning_rate, decay=decay, momentum=momentum, nesterov=True)
    model.compile(loss='categorical_crossentropy', optimizer=sgd, metrics=["accuracy"])


if __name__ == '__main__':
    params = ModelParams()
    model = build_model(params['input_shape'], params['number_of_classes'])

    compile_model(model, params['learning_rate'], params['decay'], params['momentum'])
    # serialize model to JSON
    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights("model.h5")
    json_file.close()
    model.save('model.hdf5')
    print("Saved model to disk")
