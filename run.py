# Trains a deep CNN on a subset of ImageNet data.
#
# This code was written with references to:
#   https://github.com/teammcr192/deep-CNN-keras
#   https://github.com/fchollet/keras/blob/master/examples/cifar10_cnn.py
#

import argparse
import time

from keras.models import model_from_json

from data_gen_loader import DataGenLoader
from elapsed_timer import ElapsedTimer
from img_loader import ImageLoader
from model import build_model, compile_model
from model_params import ModelParams
from report_writer import ReportWriter


def get_model(args, params):
    """
    Build the deep CNN model.
    If build is test mode then the model will be loaded from  a .json file.

    :param args: test_mode argument is defined here
    :param params: contains the input data shape, num of classes and the json filename
    :return: the build model
    """
    model = None
    # Load the model from a file or build it.
    if args.test_mode:
        f = open(params['test_model_json_file_name'], 'r')
        model = model_from_json(f.read())
        f.close()
        print('Loaded existing model from {}.'.format(params['test_model_json_file_name']))
        model.load_weights(params['test_model_weights_file_name'])
    else:
        model = build_model(params['input_shape'], params['number_of_classes'])
    return model


def test_model(args, params):
    """
    Compile the model from file and test the CNN, by running a single image
    on the compiled model. When TestImage is run the feature map is printed out.

    :param args: passed to compile_model(..)
    :param params:  parameters for learning rate, decay and momentum
    """

    # Load the model and its weights and compile it.
    model = get_model(args, params)
    print('Compiling model...')
    timer = ElapsedTimer()
    compile_model(model, params['learning_rate'], params['decay'], params['momentum'])
    print('Done in {}.'.format(timer.get_elapsed_time()))
    # Load the test images into memory and preprocess appropriately.
    print('Test data successfully loaded in {}.'.format(timer.get_elapsed_time()))
    timer.reset()
    print("Testing single image...")
    # Run the evaluation on the test data.
    from test_image import TestImage
    # predict image and show feature map and
    TestImage(model, params).predict()
    timer.reset()


def train_model(args, params, output_dir, report_writer):
    """
    Trains a model on the training data.
    The test data is used to report validation accuracy after each training epoch.

    :param args: arguments which define whetehr using data augmentation etc
    :param params: a ModelParams object containing the appropriate data file paths,
          data parameters, and training hyperparameters.
    :param output_dir: output directory of this build
    :param report_writer: for writing final build report
    """

    # Load the model and (possibly) its weights.
    model = get_model(args, params)
    # Save the model architecture
    import os
    model_arch_file = os.path.join(output_dir, params['model_json_file_name'])
    f = open(model_arch_file, 'w')
    f.write(model.to_json())
    f.close()
    print('Saved model architecture to {}.'.format(model_arch_file))
    # Compile the model.
    print('Compiling model...')
    timer = ElapsedTimer()
    compile_model(model, params['learning_rate'], params['decay'], params['momentum'])
    report_writer.write_and_print('train_time', 'Model compiled in {}.'.format(timer.get_elapsed_time()))
    # Load the images into memory and preprocess appropriately.
    timer.reset()
    if args.data_gen:
        print("Compiling model using data generator")
        data_gen_loader = DataGenLoader(params)

        total_images = params['images_per_class'] * params['number_of_classes']
        train_image_count = total_images * params['train_data_split']
        validation_image_count = total_images - train_image_count

        training_info = model.fit_generator(data_gen_loader.train_generator,
                                            steps_per_epoch=train_image_count // params['batch_size'],
                                            epochs=params['num_epochs'],
                                            validation_data=data_gen_loader.validation_generator,
                                            validation_steps=validation_image_count // params['batch_size'],
                                            shuffle=True, verbose=2)
        report_writer.write_and_print('train_time', 'Finished training model in {}.'.format(timer.get_elapsed_time()))
        timer.reset()
        print("Evaluating model...")
        train_score = model.evaluate_generator(data_gen_loader.train_generator,
                                               train_image_count // params['batch_size'])
        validation_score = model.evaluate_generator(data_gen_loader.validation_generator,
                                                    validation_image_count // params['batch_size'])
    else:
        img_loader = ImageLoader(params)
        print('Data successfully loaded in {}.'.format(timer.get_elapsed_time()))
        # Train the model.
        timer.reset()
        training_info = model.fit(img_loader.train_data, img_loader.train_labels,
                                  validation_data=(img_loader.test_data, img_loader.test_labels),
                                  batch_size=params['batch_size'], epochs=params['num_epochs'],
                                  shuffle=True, verbose=2)
        report_writer.write_and_print('train_time', 'Finished training model in {}.'.format(timer.get_elapsed_time()))
        timer.reset()
        print("Evaluating model...")
        train_score = model.evaluate(img_loader.train_data, img_loader.train_labels, verbose=0)
        validation_score = model.evaluate(img_loader.test_data, img_loader.test_labels, verbose=0)

    print('Finished evaluating model in {}.'.format(timer.get_elapsed_time()))
    timer.reset()

    # write to report and output to console
    report_writer.write_and_print('train_loss', 'Training Loss: {}'.format(train_score[0]))
    report_writer.write_and_print('train_accuracy', 'Training accuracy: {}'.format(train_score[1]))
    report_writer.write_and_print('val_loss', 'Validation Loss: {}'.format(validation_score[0]))
    report_writer.write_and_print('val_accuracy', 'Validation accuracy: {}'.format(validation_score[1]))

    # Save the models weights & model
    model_weights_file = os.path.join(output_dir, params['model_weights_file_name'])
    model_file = os.path.join(output_dir, params['model_file_name'])
    model.save_weights(model_weights_file)
    print('Saved trained model weights to {}.'.format(model_weights_file))
    model.save(model_file)
    print("Saved trained model to {}.".format(model_file))

    # show graphs
    from graphs import AccuracyAndLossGraph as graph
    adjustables = "LR={0}, DECAY={1}, MOMENTUM={2}".format(params['learning_rate'], params['decay'], params['momentum'])
    graph(training_info.history, adjustables, output_dir).draw(params['num_epochs'])

    import os

    if args.data_gen:
        from graphs import ConfusionMatrixDatGenGraph as cmgraph
        cmgraph(model, data_gen_loader.validation_generator, validation_image_count, params['batch_size'],
                output_dir, params).draw(report_writer)
    else:
        from graphs import ConfusionMatrixGraph as cmgraph
        cmgraph(model, img_loader.test_data, img_loader.test_labels, os.listdir(params['data_directory']),
                output_dir, params).draw(report_writer)

    from keras_to_tensorflow_model import KerasToTensorflowModel
    KerasToTensorflowModel(output_dir, params).write_to_pb_file(report_writer)
    print("Converted keras model to tensorflow protobuffer file")

    report_writer.write_to_file()
    print("Report written to file")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='Run a deep convolusional neural network model using Keras.')
    parser.add_argument('params_file',
                        help='The file containing data paths and model params is required.')
    parser.add_argument('--test', dest='test_mode', action='store_true',
                        help='Test the model with weights (-load-weights).')
    parser.add_argument('--datagen', dest='data_gen', action='store_true',
                        help='Use keras data generator to augment data on the fly.')

    args = parser.parse_args()
    params = ModelParams()
    if not params.read_config_file(args.params_file):
        print('Missing configuration values. Cannot continue.')
        exit(0)

    if args.test_mode:
        test_model(args, params)
    else:
        import os

        output_dir = os.path.join(os.getcwd(), "builds", time.strftime("%d-%m-%Y-%H_%M"))
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)

        report_writer = ReportWriter(output_dir)
        report_writer.params = params

        from keras import backend as K

        K.set_learning_phase(1)  # set learning phase
        train_model(args, params, output_dir, report_writer)
