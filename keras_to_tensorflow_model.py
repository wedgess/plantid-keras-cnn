"""
The following code snippet will convert the keras model file,
which is saved using model.save('kerasmodel_weight_file'),
to the freezed .pb tensorflow weight file which holds both the
network architecture and its associated weights.

Original concept of conversion from: Amir H. Abdi
Modified: Gareth Williams 2018
"""

import os

import tensorflow as tf
from keras import backend as K
from keras.models import load_model

# num_output: this value has nothing to do with the number of classes, batch_size, etc.,
# and it is mostly equal to 1.
# If you have a multi-stream network (forked network with multiple outputs),
# set the value to the number of outputs.
num_output = 1

class KerasToTensorflowModel():

    def __init__(self, output_dir, params):
        self.output_directory = output_dir
        self.loaded_model = load_model(os.path.join(output_dir, params['model_file_name']))
        K.set_learning_phase(0)

    def write_to_pb_file(self, report_writer):
        pred = [None] * num_output
        pred_node_names = [None] * num_output
        for i in range(num_output):
            pred_node_names[i] = "output_node{}".format(str(i))
            pred[i] = tf.identity(self.loaded_model.outputs[i], name=pred_node_names[i])

        # #### [optional] write graph definition in ascii
        sess = K.get_session()
        # #### convert variables to constants and save
        from tensorflow.python.framework import graph_util
        from tensorflow.python.framework import graph_io

        model_output = os.path.join(self.output_directory, 'model.pb')
        constant_graph = graph_util.convert_variables_to_constants(sess, sess.graph.as_graph_def(), pred_node_names)
        graph_io.write_graph(constant_graph, self.output_directory, model_output, as_text=False)
        print('Saved the frozen graph (ready for inference) at: ', model_output)
        self.print_graph_node_info(model_output, report_writer)
        self.create_tensorboard_log(model_output)

    def print_graph_node_info(self, protobuffer_file_path, report_writer):
        graph = tf.GraphDef()
        graph.ParseFromString(open(protobuffer_file_path, 'rb').read())
        report_writer.__setattr__('input', str([n for n in graph.node if n.name.find('input') != -1][0]).replace('name', 'input_node_name').splitlines()[0])
        report_writer.__setattr__('output', str([n for n in graph.node if n.name.find('output') != -1][0]).replace('name', 'output_node_name').splitlines()[0])


    def create_tensorboard_log(self, protobuffer_path):
        from tensorflow.python.platform import gfile
        with tf.Session() as sess:
            with gfile.FastGFile(protobuffer_path, 'rb') as f:
                graph_def = tf.GraphDef()
                graph_def.ParseFromString(f.read())
                tf.import_graph_def(graph_def)
        train_writer = tf.summary.FileWriter(self.output_directory)
        train_writer.add_graph(sess.graph)
        train_writer.flush()
        train_writer.close()