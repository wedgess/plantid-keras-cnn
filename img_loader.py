import os

import cv2
import numpy as np
from keras.utils import np_utils
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle


class ImageLoader(object):
    """
    Used for getting the training and validation data from their folders and performing the split.
    :param params: - image parameters
    """

    def __init__(self, params):
        self.data_directory = os.path.join(os.getcwd(), params['data_directory'])
        self.img_dimens = params['input_shape']
        self.train_split = params['train_data_split']
        self.imgs_per_class = params['images_per_class']
        self.img_data = self.load_images()
        self.num_classes = params['number_of_classes']
        self.perform_train_validation_split()

    def load_images(self):
        # Define data path
        data_dir_list = os.listdir(self.data_directory)

        # image data as a list
        img_data_list = []

        print('-- Loading datasets')
        for dataset in data_dir_list:
            class_dir = os.path.join(self.data_directory, dataset)
            print('- Loaded images from dataset - {}'.format(dataset))
            if len(os.listdir(class_dir)) != self.imgs_per_class:
                print("[ERROR] Expected {0} images in {1} but found {2} images".format(self.imgs_per_class, dataset,
                                                                                       len(os.listdir(class_dir))))
                exit(0)
            for img in os.listdir(class_dir):
                input_img = cv2.imread(os.path.join(class_dir, img))
                input_img_resized = cv2.resize(input_img, (self.img_dimens[0], self.img_dimens[1]))
                img_data_list.append(input_img_resized)

        img_data = np.array(img_data_list)
        img_data = img_data.astype('float32')
        img_data /= 255
        print("-- Image data shape: {}.".format(img_data.shape))
        return img_data

    def perform_train_validation_split(self):
        print("-- Splitting dataset")
        num_of_samples = self.img_data.shape[0]
        labels = np.ones((num_of_samples,), dtype='int64')  # create length of labels array

        # split labels per num images sin class
        for i in range(0, self.num_classes):
            labels[(i * self.imgs_per_class):((i + 1) * self.imgs_per_class)] = i

        # convert class labels to one-hot encoding
        Y = np_utils.to_categorical(labels, self.num_classes)
        # Shuffle the dataset
        x, y = shuffle(self.img_data, Y, random_state=2)
        # Split the dataset - train(80%) and test(20%)
        self.train_data, self.test_data, self.train_labels, self.test_labels = train_test_split(x, y, test_size=(1 - self.train_split),
                                                                                                random_state=2)
