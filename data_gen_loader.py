from keras.preprocessing.image import ImageDataGenerator


class DataGenLoader():
    """
    Builds the Keras data generators for training and validation data.
    :param params - contains rge main parameters such as batch_size, train, validate, img dimens
    """

    def __init__(self, params):
        img_width = params['input_shape'][0]
        img_height = params['input_shape'][1]
        batch_size = params['batch_size']
        train_directory = params['data_gen_train_directory']
        validate_directory = params['data_gen_validate_directory']

        self.train_generator = self.create_training_generator().flow_from_directory(
            train_directory,
            target_size=(img_width, img_height),
            batch_size=batch_size,
            class_mode='categorical')

        self.validation_generator = self.create_validation_generator().flow_from_directory(
            validate_directory,
            target_size=(img_width, img_height),
            batch_size=batch_size,
            class_mode='categorical')

    def create_training_generator(self):
        return ImageDataGenerator(
            rescale=1. / 255,
            shear_range=0.2,
            zoom_range=0.2,
            rotation_range=90,
            width_shift_range=0.1,
            height_shift_range=0.1,
            horizontal_flip=True)

    def create_validation_generator(self):
        return ImageDataGenerator(rescale=1. / 255)
